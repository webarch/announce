# Webarchitects Announcements Ansible Role

This Ansible role is designed to be used with the [users role](https://git.coop/webarch/users), to send announcement emails to users on shared servers, for example, to inform users that they should expect some downtime as a server is due to be upgraded.

## Usage

This role can be added to a `requirements.yml` file and then run via a file such as the following example `announce.yml` file:

```yml
---
- name: Send an announcement email
  become: true

  hosts:
    - wsh_jessie_servers

  vars:
    announce_subject: IMPORTANT Impending Debian Jessie Server and PHP Upgrades
    announce_body: ~/webarch/announce/templates/jessie_eol.j2

  roles:
    - announce
...
```

Note that the `announce_body` should be an absolute path to a template on the Ansible controller (the machine that Ansible is being run on).

To do a dry run:

```bash
ansible-galaxy install -r requirements.yml --force announce
ansible-playbook announce.yml --check
```

This will write a copy of every email that would be sent to a `.check` directory as a sub-directory of the one created for the sent emails.

To send the email:

```bash
ansible-galaxy install -r requirements.yml --force announce
ansible-playbook announce.yml
```

This will save a copy of every email sent to the sent directory on the server and if it detects that the sent file already exists it won't send the email twice.

<!-- BEGIN_ANSIBLE_DOCS -->

## Requirements

| Platform | Versions |
| -------- | -------- |
| Debian | bookworm, bullseye |
| Ubuntu | jammy |

## Defaults

See [defaults/main.yml](defaults/main.yml).

## Role Variables

### Entrypoint: main

The main entry point for the annonce Ansible role.

|Option|Description|Type|Required|Default|
|---|---|---|---|---|
| announce | Run the tasks in this role. | bool | yes | false |
| announce_body | A path to a template for the announcement email body. | str | yes |  |
| announce_from | The From address for the announcement emails. | str | yes |  |
| announce_headers | A optional list of additional email headers. | list of 'str' | no |  |
| announce_sent | A path for copies of sent emails. | str | yes |  |
| announce_signature | An optional email signature. | str | no |  |
| announce_subject | A Subject for the announcement email. | str | yes |  |
| announce_subject_tag | An optional email subject tag. | str | no |  |

## Dependencies

None.

<!-- END_ANSIBLE_DOCS -->
